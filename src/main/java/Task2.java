import java.util.Arrays;

public class Task2 {

    public static void main(String[] args) {
        String[] array1 = {"Hello", "hello", "a", "0", "007 - agent", "agent - 007", "", "WORLD", "world", "(*(^_^)*)"};
        selectionSort(array1);
        System.out.println("Selection Sort: \t" + Arrays.toString(array1));

        String[] array2 = {"Hello", "hello", "a", "0", "007 - agent", "agent - 007", "", "WORLD", "world", "(*(^_^)*)"};
        bubbleSort(array2);
        System.out.println("Bubble Sort: \t\t" + Arrays.toString(array2));

        String[] array3 = {"Hello", "hello", "a", "0", "007 - agent", "agent - 007", "", "WORLD", "world", "(*(^_^)*)"};
        insertionSort(array3);
        System.out.println("Insertion Sort: \t" + Arrays.toString(array3));
    }

    public static void selectionSort(String[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            int minIndex = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j].compareTo(array[minIndex]) < 0) {
                    minIndex = j;
                }
            }
            swap(array, i, minIndex);
        }
    }

    public static void bubbleSort(String[] array) {
        for (int i = 1; i < array.length; i++) {
            for (int j = 0; j < array.length - i; j++) {
                if (array[j].compareTo(array[j + 1]) > 0) {
                    swap(array, j, j + 1);
                }
            }
        }
    }

    public static void insertionSort(String[] array) {
        for (int i = 1; i < array.length; i++) {
            int j = i;
            String temp = array[i];
            while (j > 0 && array[j - 1].compareTo(temp) > 0) {
                array[j] = array[j - 1];
                j--;
            }
            array[j] = temp;
        }
    }

    private static void swap(String[] array, int index1, int index2) {
        String temp = array[index1];
        array[index1] = array[index2];
        array[index2] = temp;
    }
}
