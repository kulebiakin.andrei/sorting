import java.util.Arrays;

public class Task1 {

    public static void main(String[] args) {
        char[] array1 = {'z', 'c', 'd', 'a', 'Z', 'w', 'l', 'A', 'b', 'B', 'y'};
        selectionSort(array1);
        System.out.println("Selection Sort: \t" + Arrays.toString(array1));

        char[] array2 = {'z', 'c', 'd', 'a', 'Z', 'w', 'l', 'A', 'b', 'B', 'y'};
        bubbleSort(array2);
        System.out.println("Bubble Sort: \t\t" + Arrays.toString(array2));

        char[] array3 = {'z', 'c', 'd', 'a', 'Z', 'w', 'l', 'A', 'b', 'B', 'y'};
        insertionSort(array3);
        System.out.println("Insertion Sort: \t" + Arrays.toString(array3));
    }

    public static void selectionSort(char[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            int minIndex = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < array[minIndex]) {
                    minIndex = j;
                }
            }
            swap(array, i, minIndex);
        }
    }

    public static void bubbleSort(char[] array) {
        for (int i = 1; i < array.length; i++) {
            for (int j = 0; j < array.length - i; j++) {
                if (array[j] > array[j + 1]) {
                    swap(array, j, j + 1);
                }
            }
        }
    }

    public static void insertionSort(char[] array) {
        for (int i = 1; i < array.length; i++) {
            int j = i;
            char temp = array[i];
            while (j > 0 && array[j - 1] > temp) {
                array[j] = array[j - 1];
                j--;
            }
            array[j] = temp;
        }
    }

    private static void swap(char[] array, int index1, int index2) {
        char temp = array[index1];
        array[index1] = array[index2];
        array[index2] = temp;
    }
}
