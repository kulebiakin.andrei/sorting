import java.util.Arrays;

public class Task3 {

    public static void main(String[] args) {
        String[] names = {"Egor", "Artiom", "Egor", "Vlad", "Igor", "Ivan", "Kirill"};
        String[] surnames = {"Anufriev", "Abikenov", "Kalinchuk", "Ilyin", "Loginov", "Minin", "Fomichev"};

        sortStudents(names, surnames);
        System.out.println(Arrays.toString(names));
        System.out.println(Arrays.toString(surnames));
    }

    public static void sortStudents(String[] names, String[] surnames) {
        // Using Selection Sort with an additional condition
        for (int i = 0; i < names.length - 1; i++) {
            int minIndex = i;
            for (int j = i + 1; j < names.length; j++) {
                int compareNames = names[j].compareTo(names[minIndex]);
                int compareSurnames = surnames[j].compareTo(surnames[minIndex]);
                if (compareNames < 0 || (compareNames == 0 && compareSurnames < 0)) {
                    minIndex = j;
                }
            }

            if (minIndex != i) {
                swap(names, i, minIndex);
                swap(surnames, i, minIndex);
            }
        }
    }

    private static void swap(String[] array, int index1, int index2) {
        String temp = array[index1];
        array[index1] = array[index2];
        array[index2] = temp;
    }
}
